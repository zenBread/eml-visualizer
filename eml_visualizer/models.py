from gqlalchemy import Node
from gqlalchemy import Relationship


class User(Node):
    id: str
    username: str | None
    email: str | None
    filename: str | None


class Email(Node):
    id: str
    message_id: list[str] | None
    From: list[str] | None
    To: list[str] | None
    date: str | None
    subject: str | None
    content_type: str | None
    filename: str | None


class IP(Node):
    id: str
    ip: str | None


class EmailDomains(Node):
    id: str
    domains: list[str] | None
    ips: list[str] | None
    software: str | None
    alias: str | None


# Relationships
class Received(Relationship, type="RECEIVED"):
    date: str


class Sent(Relationship, type="SENT"):
    date: str


class Emailed(Relationship, type="EMAILED"):
    date: str
