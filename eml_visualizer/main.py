import argparse
import eml_parser
import logging
import os

from datetime import datetime
from dotenv import load_dotenv
from gqlalchemy import Memgraph
from eml_visualizer.models import Email
from eml_visualizer.models import Emailed
from eml_visualizer.models import Received
from eml_visualizer.models import Sent
from eml_visualizer.models import User
from typing import TYPE_CHECKING, Any


load_dotenv()

HOST = os.getenv("HOST", "localhost")
PORT = os.getenv("BOLT_PORT", "7687")

logger = logging.getLogger(__name__)


def parse_eml(folder: str) -> list[dict[str, Any]]:
    ep = eml_parser.EmlParser()
    emails = []
    for dirpath, _, filenames in os.walk(folder):
        for filename in filenames:
            if filename.endswith(".eml") or filename.endswith(".mai"):
                fpath = os.path.join(dirpath, filename)
                with open(fpath, "rb") as fh:
                    parsed = ep.decode_email_bytes(fh.read())
                    parsed.update({"filename": fpath})
                    emails.append(parsed)
    return emails


def get_graph(emails: list[dict[str, Any]], db: Memgraph) -> None:
    print("Adding to graph")
    count = 1
    for email in emails:

        if header := email.get("header"):
            email_node = Email(id=count)
            count += 1
            email_node.message_id = header.get("message-id", [])
            email_node.subject = header.get("subject", "")
            date: datetime = header.get("date")
            email_node.date = date.isoformat()
            email_node.From = header.get("from")
            email_node.To = header.get("to")
            email_node.content_type = header.get("content-type", "text/plain")
            email_node.filename = header.get("filename")
            to_users = []
            from_users = []
            if not isinstance(email_node.To, list):
                email_node.To = [email_node.To]
            if not isinstance(email_node.From, list):
                email_node.From = [email_node.From]
            for t in email_node.To:
                username = t.split("@")[0]
                to_users.append(
                    User(
                        id=count,
                        username=username,
                        email=t,
                        filename=email_node.filename,
                    )
                )
                count += 1
            if cc := header.get("cc", []):
                for c in cc:
                    username = c.split("@")[0]
                    to_users.append(
                        User(
                            id=count,
                            username=username,
                            email=c,
                            filename=email_node.filename,
                        )
                    )
                    count += 1
            for f in email_node.From:
                username = f.split("@")[0]
                from_users.append(
                    User(
                        id=count,
                        username=username,
                        email=f,
                        filename=email_node.filename,
                    )
                )
                count += 1
            e = email_node.save(db)
            for user in to_users:
                u = user.save(db)
                Received(
                    _start_node_id=u._id, _end_node_id=e._id, date=email_node.date
                ).save(db)
            for user in from_users:
                u = user.save(db)
                Sent(
                    _start_node_id=u._id, _end_node_id=e._id, date=email_node.date
                ).save(db)

            for sender in from_users:
                for to in to_users:
                    Emailed(
                        _start_node_id=sender._id,
                        _end_node_id=to._id,
                        date=email_node.date,
                    ).save(db)


def main() -> int:
    parser = argparse.ArgumentParser(
        prog="eml-visualizer",
        description="Given folder of eml files, parse and relate the data in a local memgraph container.",
    )
    parser.add_argument("-f", "--folder", help="Folder path", required=True)

    args = parser.parse_args()

    try:
        emails = parse_eml(args.folder)
    except OSError as err:
        logger.error("Unable to read files: %s" % err)
        return 5

    if not emails:
        logger.info("No emails in folder")
        return 0

    try:
        db = Memgraph(host=HOST, port=int(PORT))

        get_graph(emails=emails, db=db)

    except Exception as err:
        logger.error(err)
        return 1
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
