# Creating virtual environment

## Virtualenv

> Make sure to install virtualenv

Linux: `sudo apt install python3-virtualenv`

```bash
virtualenv venv
source venv/bin/activate
pip install .
```

## Running

```bash
docker compose up -d
emlvis -f data
```

> localhost:3000

```bash
# Query
MATCH (n)-[e]-()
WITH collect(n) AS nodes, collect(e) AS edges
CALL wcc.get_components(nodes, edges)
YIELD *
RETURN *
```
